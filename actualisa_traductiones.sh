#!/bin/sh

LINGUAS="en it ru"
LUPDATE="${LUPDATE:=lupdate}"

TS_PARAMS=""
for L in $LINGUAS
do
    TS_PARAMS="$TS_PARAMS -ts data/i18n/studentario_${L}.ts"
done

"$LUPDATE" -no-obsolete -source-language ia \
    src/ \
    $TS_PARAMS
