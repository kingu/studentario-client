import qbs

Module {
    cpp.driverFlags: project.enableCoverage ? ["--coverage"] : []
    cpp.enableExceptions: false
    cpp.enableRtti: false

    Depends { name: "cpp" }
}
