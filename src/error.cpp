/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "error.h"

#include <QDebug>

using namespace Studentario;

Error::Error(QObject *parent):
    QObject(parent)
{
}

Error::~Error() = default;

QString Error::messageFromCode(int code)
{
    if (code >= 1000) {
        // Errores definite del cliente
        if (code == 1000) {
            return tr("Le operation ha fallite. "
                      "Assecura te de esser connectite a internet");
        }
    }

    // Mantene isto synchronisate con lo de studentario-server
    static const char *messages[] = {
        QT_TR_NOOP("Nulle error"),
        QT_TR_NOOP("Error incognite"),
        QT_TR_NOOP("Error del base de datos"),
        QT_TR_NOOP("Il manca un campo necessari"),
        QT_TR_NOOP("Accesso refusate"),
        QT_TR_NOOP("Nomine de usator non trovate"),
        QT_TR_NOOP("Contrasigno errate"),
        QT_TR_NOOP("On necessita le authentication"),
        QT_TR_NOOP("Parametros invalide"),
        QT_TR_NOOP("Usator non trovate"),
        QT_TR_NOOP("Gruppo non trovate"),
    };

    constexpr int messageCount = sizeof(messages) / sizeof(QString);
    return code >= 0 && code < messageCount ?
        tr(messages[code]) : QString();
}
