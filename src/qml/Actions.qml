import QtQml 2.12
import QtQuick 2.12
import QtQuick.Controls 2.12
import it.mardy.studentario 1.0

QtObject {
    property var site: null

    property var manageStudents: Action {
        text: qsTr("Administra le studentes")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Student,
        })
    }

    property var manageTeachers: Action {
        text: qsTr("Administra le inseniantes")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Teacher,
        })
    }

    property var manageAdmins: Action {
        text: qsTr("Administra le administratores")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Admin,
        })
    }

    property var manageDirectors: Action {
        text: qsTr("Administra le directores")
        onTriggered: stackView.push(Qt.resolvedUrl("UsersPage.qml"), {
            'site': site,
            'editable': true,
            'role': Roles.Director,
        })
    }
}
