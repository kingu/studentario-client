import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property int role: -1
    property int userId: -1
    property string name: ""

    signal done()

    property bool _editing: userId >= 0
    property var _availableRoles: []
    property bool _canLogin: site.roleCanLogin(role)

    header: TitleHeader {}
    title: {
        if (_editing) {
            switch (root.role) {
            case Roles.Student: return qsTr("Modifica studente")
            case Roles.Parent: return qsTr("Modifica parente (accompaniator)")
            case Roles.Teacher: return qsTr("Modifica inseniante")
            case Roles.Admin: return qsTr("Modifica administrator")
            case Roles.Director: return qsTr("Modifica director")
            }
        } else {
            switch (root.role) {
            case Roles.Student: return qsTr("Nove studente")
            case Roles.Parent: return qsTr("Nove parente (accompaniator)")
            case Roles.Teacher: return qsTr("Nove inseniante")
            case Roles.Admin: return qsTr("Nove administrator")
            case Roles.Director: return qsTr("Nove director")
            }
        }
    }

    Component.onCompleted: {
        if (_editing) {
            site.loadUser(userId, function(reply) {
                if (reply.status == 200) {
                    var user = reply.json.data
                    loginField.text = user.login
                    contactInfoEditor.contactInfo = user.contactInfo
                    keywordsField.text = user.keywords
                    _availableRoles = site.availableRolesForUser(user, role)
                }
            })
        } else {
            _availableRoles = site.availableRolesForUser({}, role)
        }
        nameField.forceActiveFocus(Qt.TabFocusReason)
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: width

        ColumnLayout {
            anchors {
                fill: parent
                leftMargin: 12; rightMargin: 12
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Nomine complete")
            }
            TextField {
                id: nameField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                text: root.name
            }

            GroupBox {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                title: qsTr("Informationes de accesso")
                visible: root._canLogin

                resources: [
                    Switch {
                        id: editingLoginSwitch
                        text: qsTr("Modifica le datos de accesso")
                        visible: root._editing
                        checked: !visible
                    }
                ]
                Component.onCompleted: if (root._editing) label = editingLoginSwitch

                ColumnLayout {
                    anchors.fill: parent
                    enabled: editingLoginSwitch.checked

                    HelperLabel {
                        Layout.fillWidth: true
                        text: qsTr("Identificativo de accesso (p.ex. e-posta, telephono)")
                    }
                    TextField {
                        id: loginField
                        Layout.fillWidth: true
                        Layout.bottomMargin: 10
                    }

                    HelperLabel {
                        Layout.fillWidth: true
                        text: root._editing ?
                            qsTr("Nove contrasigno (vacue pro non cambiar)") :
                            qsTr("Contrasigno")
                    }
                    TextField {
                        id: passwordField
                        Layout.fillWidth: true
                        placeholderText: root._editing ?
                            qsTr("Mantene le contrasigno currente") :
                            qsTr("Scribe le contrasigno del usator")
                    }
                }
            }

            GroupBox {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                title: qsTr("Rolos")
                visible: rolesRepeater.count > 1

                Flow {
                    anchors.fill: parent
                    Repeater {
                        id: rolesRepeater
                        model: root._availableRoles
                        CheckBox {
                            text: qsTr(modelData.name)
                            checked: modelData.active
                            enabled: modelData.enabled
                        }
                    }
                }
            }

            GroupBox {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                title: qsTr("Informationes de contacto")

                ContactInfoEditor {
                    id: contactInfoEditor
                    anchors.fill: parent
                    editable: true
                }
            }

            HelperLabel {
                Layout.fillWidth: true
                text: qsTr("Parolas clave")
            }
            TextField {
                id: keywordsField
                Layout.fillWidth: true
                Layout.bottomMargin: 10
            }

            ErrorLabel {
                id: errorLabel
                Layout.fillWidth: true
            }

            Button {
                Layout.topMargin: 12
                Layout.bottomMargin: 12
                Layout.alignment: Qt.AlignHCenter
                Layout.minimumWidth: parent.width / 2
                text: qsTr("Salva")
                onClicked: root.save()
            }
        }
    }

    function save() {
        errorLabel.clear()
        if (nameField.text.length < 4) {
            errorLabel.message = qsTr("Scribe un nomine valide")
            return
        }

        var callback = function(reply) {
            if (reply.status == 200) {
                console.log("Usator salvate")
                root.done()
            } else {
                errorLabel.error = reply.json
            }
        }

        var user = {
            'name': nameField.text,
            'keywords': keywordsField.text,
            'contactInfo': contactInfoEditor.contactInfo,
        }
        for (var i = 0; i < _availableRoles.length; i++) {
            var role = _availableRoles[i]
            var enabled = rolesRepeater.itemAt(i).checked
            user[role.field] = enabled
        }

        if (editingLoginSwitch.checked) {
            if (loginField.text) user.login = loginField.text
            if (passwordField.text) user.password = passwordField.text
        }

        if (!root._editing) {
            site.createUser(user, callback)
        } else {
            site.updateUser(root.userId, user, callback)
        }
    }
}
