import QtQuick 2.12
import QtQuick.Controls 2.12
import it.mardy.studentario 1.0

Label {
    property var error: null
    property string message: ""

    property string errorMessage: {
        if (message) return message
        if (!error) return ""
        return error.message ?
            error.message : Error.messageFromCode(error.code)
    }

    color: "red"
    visible: errorMessage.length > 0
    text: qsTr("Error: %1").arg(errorMessage)
    wrapMode: Text.Wrap

    function clear() {
        error = null
        message = ""
    }
}
