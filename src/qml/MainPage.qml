import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property var site: null

    property var availableActions: []

    header: TitleHeader {
        canGoBack: false
    }
    title: qsTr("Pagina principal")

    StackView.onActivated: computeAvailableActions(site)

    ScrollView {
        anchors.fill: parent
        contentWidth: width

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; top: parent.top; margins: 12 }

            Repeater {
                model: root.availableActions

                Button {
                    Layout.fillWidth: true
                    flat: true
                    action: modelData
                }
            }
        }
    }

    Actions {
        id: actions
        site: root.site
    }

    function computeAvailableActions(site) {
        var tmp = []
        if (site.canEditStudents) {
            tmp.push(actions.manageStudents)
        }
        if (site.canEditTeachers) {
            tmp.push(actions.manageTeachers)
        }
        if (site.canEditAdmins) {
            tmp.push(actions.manageAdmins)
        }
        if (site.canEditDirectors) {
            tmp.push(actions.manageDirectors)
        }
        availableActions = tmp
    }
}
