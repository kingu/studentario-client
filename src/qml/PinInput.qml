import QtQuick 2.12
import QtQuick.Controls 2.12

TextField {
    id: root

    property var errorLabel: null

    echoMode: TextInput.Password
    inputMethodHints: Qt.ImhDigitsOnly | Qt.ImhSensitiveData
    inputMask: "9999"
    maximumLength: 4
    passwordMaskDelay: 1500
    horizontalAlignment: Qt.AlignHCenter
    font {
        pointSize: 24
        letterSpacing: 2
    }

    onTextChanged: if (text.length == maximumLength) {
        accepted()
    }

    function validatedPin() {
        var pin = root.acceptableInput ? root.text : ""
        if (pin.length < 4) {
            root.errorLabel.message = qsTr("Le PIN es troppo curte!")
            root.forceActiveFocus()
            return ""
        }
        return pin
    }
}
