import Qt.labs.settings 1.0
import QtQml 2.12
import it.mardy.studentario 1.0
import "Permissions.js" as Permissions

QtObject {
    id: root

    readonly property string displayName: settings.displayName
    readonly property url baseUrl: settings.baseUrl
    property string authenticationToken: ""
    property var authenticatedUser: null
    property bool waiting: false
    property bool canEditStudents: isAtLeast(Roles.Admin)
    property bool canEditTeachers: isAtLeast(Roles.Director)
    property bool canEditAdmins: isAtLeast(Roles.Director)
    property bool canEditDirectors: isAtLeast(Roles.Master)

    signal authenticated()
    signal authenticationFailed(var error)

    property var configFile: FileIO {
        filePath: root.configFilePath()
    }

    property var settings: JSON.parse(configFile.contents)

    function isAtLeast(role) {
        return Permissions.userIsAtLeast(root.authenticatedUser, role)
    }

    function roleCanLogin(role) {
        // In futuro, isto essera configurabile in le sito
        return role == Roles.Master ||
            role == Roles.Director ||
            role == Roles.Admin ||
            role == Roles.Teacher
    }

    function availableRolesForUser(user, mainRole) {
        return Permissions.grantableRoles(root.authenticatedUser,
                                          user, mainRole)
    }

    function configFilePath() {
        var args = Qt.application.arguments
        return args.length > 1 ? args[1] : ":/studentario.conf"
    }

    function request(verb, path, params, callback) {
        var http = new XMLHttpRequest()

        http.open(verb, root.baseUrl + path, true)
        http.setRequestHeader('Content-type','application/json; charset=utf-8')
        if (root.authenticationToken) {
            http.setRequestHeader('Authorization',
                                  'Basic ' + root.authenticationToken)
        }
        http.onreadystatechange = function() {
            if (http.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                console.log("Response Headers -->")
                console.log(http.getAllResponseHeaders())
            } else if (http.readyState === XMLHttpRequest.DONE) {
                root.waiting = false

                var reply = {
                    'status': http.status,
                }
                var json = {}
                try {
                    json = JSON.parse(http.responseText)
                } catch (e) {
                    console.log('Failed to parse reply: ' + http.responseText)
                    json = {
                        'status': 'error',
                        'code': 1000, // nulle internet
                    }
                }
                reply['json'] = json
                callback(reply)
            }
        }
        root.waiting = true
        http.send(JSON.stringify(params))
    }

    function post(path, params, callback) {
        request('POST', path, params, callback)
    }

    function put(path, params, callback) {
        request('PUT', path, params, callback)
    }

    function get(path, query, callback) {
        var fullPath = path
        if (query) {
            fullPath += "?" + query
        }
        request('GET', fullPath, {}, callback)
    }

    function remove(path, callback) {
        request('DELETE', path, {}, callback)
    }

    function login(userName, password, pin) {
        var params = {
            'login': userName,
        }
        if (password) params['password'] = password
        if (pin) params['pinCode'] = pin

        var callback = function(reply) {
            console.log("Login reply: " + JSON.stringify(reply))
            if (reply.status == 200) {
                var data = reply.json.data
                root.authenticationToken = data.authenticationToken
                root.authenticatedUser = data.user
                root.authenticated()
            } else {
                root.authenticationFailed(reply.json)
            }
        }
        post("/api/v1/login", params, callback)
    }

    function updatePin(password, newPin, callback) {
        var params = {
            'password': password,
            'newPin': newPin,
        }
        post("/api/v1/users/me/updatePin", params, callback)
    }

    function queryUsers(role, callback) {
        get("/api/v1/users", "role=" + role, callback)
    }

    function loadUser(userId, callback) {
        get("/api/v1/users/" + userId, "", callback)
    }

    function createUser(user, callback) {
        post("/api/v1/users", user, callback)
    }

    function updateUser(userId, user, callback) {
        put("/api/v1/users/" + userId, user, callback)
    }

    function deleteUser(userId, callback) {
        remove("/api/v1/users/" + userId, callback)
    }
}
