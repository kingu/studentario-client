import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

SwipeDelegate {
    id: root

    property bool editable: false
    property alias name: root.text

    signal deletionRequested()

    swipe.right: Button {
        anchors.right: parent.right
        height: parent.height
        visible: root.editable
        flat: true
        icon {
            source: "qrc:/icons/delete"
            color: "transparent"
        }
        onClicked: root.deletionRequested()
    }
}
