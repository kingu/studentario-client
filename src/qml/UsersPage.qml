import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.studentario 1.0

Page {
    id: root

    property var site: null
    property bool editable: false
    property int role: -1

    header: TitleHeader {}
    title: {
        switch (root.role) {
        case Roles.Student: return qsTr("Studentes")
        case Roles.Parent: return qsTr("Parentes")
        case Roles.Teacher: return qsTr("Inseniantes")
        case Roles.Admin: return qsTr("Administratores")
        case Roles.Director: return qsTr("Directores")
        }
    }
    onRoleChanged: refresh()
    
    ListView {
        id: listView
        anchors.fill: parent
        header: createUserComponent
        delegate: UserDelegate {
            width: listView.width
            editable: root.editable
            name: modelData.name
            onDeletionRequested: root.deleteUser(modelData.userId, modelData.name)
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditUser.qml"), {
                    'site': root.site,
                    'role': root.role,
                    'userId': modelData.userId,
                    'name': modelData.name,
                })
                page.done.connect(function() {
                    root.refresh()
                    root.StackView.view.pop()
                })
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }

    Component {
        id: createUserComponent
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            text: {
                switch (root.role) {
                case Roles.Student: return qsTr("Adde un nove studente")
                case Roles.Parent: return qsTr("Adde un nove parente")
                case Roles.Teacher: return qsTr("Adde un nove inseniante")
                case Roles.Admin: return qsTr("Adde un nove administrator")
                case Roles.Director: return qsTr("Adde un nove director")
                }
            }
            onClicked: {
                var page = stackView.push(Qt.resolvedUrl("EditUser.qml"), {
                    'site': root.site,
                    'role': root.role,
                })
                page.done.connect(function() {
                    root.refresh()
                    root.StackView.view.pop()
                })
            }
        }
    }

    Dialog {
        id: deleteConfirmationDialog
        property string userName: ""
        property int userId: -1

        title: qsTr("Confirma le remotion")
        standardButtons: Dialog.Yes | Dialog.No
        modal: true
        anchors.centerIn: parent

        Label {
            text: qsTr("Esque tu es secur que tu vole deler «%1»?").
                arg(deleteConfirmationDialog.userName)
            wrapMode: Text.Wrap
        }

        onAccepted: {
            site.deleteUser(userId, function(reply) {
                if (reply.status == 200) {
                    root.refresh()
                }
            })
        }
    }

    function deleteUser(userId, userName) {
        deleteConfirmationDialog.userName = userName
        deleteConfirmationDialog.userId = userId
        deleteConfirmationDialog.open()
    }

    function refresh() {
        var r = Roles.info(root.role)
        site.queryUsers(r.id, function(reply) {
            if (reply.status == 200) {
                listView.model = reply.json.data
            }
        })
    }
}
