import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ApplicationWindow {
    id: root

    width: 480
    height: 320
    visible: true
    title: qsTr("Studentario")

    onActiveFocusControlChanged: console.log("Active focus control: " + activeFocusControl)

    StackView {
        id: stackView
        anchors { left: parent.left; right: parent.right; top: parent.top; bottom: osk.top }
        initialItem: loginPage
        Keys.onBackPressed: if (depth > 1 && currentItem.objectName != "mainPage") {
            event.accepted = true
            stackView.pop()
        } else {
            event.accepted = false
        }
    }

    Component {
        id: loginPage
        LoginPage {
            site: siteObject
            onAuthenticated: {
                if (password) {
                    stackView.push(pinCreationPage, {
                        'password': password,
                    })
                } else {
                    stackView.push(mainPage)
                }
            }
        }
    }

    Component {
        id: pinCreationPage
        PinCreationPage {
            site: siteObject
            onDone: stackView.push(mainPage)
        }
    }

    Component {
        id: mainPage
        MainPage {
            objectName: "mainPage"
            site: siteObject
        }
    }

    Site { id: siteObject }

    KeyboardRectangle { id: osk }
}
