/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"

#include "error.h"
#include "file_io.h"
#include "json_storage.h"
#include "roles.h"
#include "standard_paths.h"
#include "temporary_dir.h"

#include <QDebug>
#include <QQmlEngine>

using namespace Studentario;

void Studentario::registerTypes()
{
    qmlRegisterSingletonType<Error>(
        "it.mardy.studentario", 1, 0, "Error",
        [](QQmlEngine *, QJSEngine *) -> QObject * {
            return new Error();
        });

    qmlRegisterType<LinguaLonga::FileIO>("it.mardy.studentario",
                                         1, 0, "FileIO");
    qmlRegisterType<LinguaLonga::JsonStorage>("it.mardy.studentario",
                                              1, 0, "JsonStorage");
    qmlRegisterType<LinguaLonga::StandardPaths>("it.mardy.studentario",
                                                1, 0, "StandardPaths");
    qmlRegisterSingletonType<Error>(
        "it.mardy.studentario", 1, 0, "Roles",
        [](QQmlEngine *, QJSEngine *) -> QObject * {
            return new Roles();
        });
    qmlRegisterType<LinguaLonga::TemporaryDir>("it.mardy.studentario",
                                               1, 0, "TemporaryDir");
}
