import qbs 1.0

Project {
    name: "studentario"

    property bool buildTests: false
    property bool enableCoverage: false

    references: [
        "tests/tests.qbs",
    ]
    qbsSearchPaths: "qbs"

    QtGuiApplication {
        name: "it.mardy.studentario.client"
        targetName: "studentario"
        install: true

        files: [
            "data/i18n/*.ts",
            "data/icons/icons.qrc",
            "src/error.cpp",
            "src/error.h",
            "src/file_io.cpp",
            "src/file_io.h",
            "src/json_storage.cpp",
            "src/json_storage.h",
            "src/main.cpp",
            "src/roles.cpp",
            "src/roles.h",
            "src/standard_paths.cpp",
            "src/standard_paths.h",
            "src/temporary_dir.cpp",
            "src/temporary_dir.h",
            "src/types.cpp",
            "src/types.h",
        ]

        Group {
            files: [
                "data/qtquickcontrols2.conf",
                "data/studentario.conf",
                "src/qml/*.js",
                "src/qml/*.qml",
            ]
            fileTags: ["qt.core.resource_data"]
        }

        Group {
            fileTagsFilter: "qm"
            fileTags: ["qt.core.resource_data"]
            Qt.core.resourcePrefix: "/i18n/"
        }

        Depends { name: "Qt.quick" }
        Depends { name: "buildconfig" }
    }

    AutotestRunner {
        name: "check"
        environment: [ "QT_QPA_PLATFORM=offscreen" ]
        Depends { productTypes: ["coverage-clean"] }
    }
}
