#! /bin/sh

SERVER_BUILD_DIR="${1:-../studentario-server/build/}"

CONFIG_FILE="$(mktemp /tmp/studentario.conf.XXXXXX)"

cat > $CONFIG_FILE <<-EOF
[Cutelyst]
DatabaseName = "$HOME/studentario.db"
Testing = true
EOF

createMaster () {
    sleep 2
    curl -s \
        -H "Content-Type: application/json" \
        -X POST \
        http://localhost:3000/testing/createMaster
}

createMaster &

cleanUp() {
    echo "Cleaning up."
    rm "$CONFIG_FILE"
}

trap cleanUp EXIT INT TERM

cd "$SERVER_BUILD_DIR"
cutelyst2 -r --server --app-file src/libStudentario.so -- --chdir .. --ini "$CONFIG_FILE"

trap - EXIT
